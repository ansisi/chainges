import Swiper from 'swiper/dist/js/swiper.js';
import {  browser } from '@superskrypt/sutils';

browser.on( 'browser:ready', () => {
    var swiper = new Swiper ('.swiper-container', {
        direction: 'horizontal',
        loop: true,
        slidesPerView: 'auto',
        centeredSlides: true,
        slidesPerGroup: 1,
        speed: 1000,
        autoplay: {
            delay: 4000,
            disableOnInteraction: true,
        },
        navigation: {
            nextEl: '.swiper-slide-next',
            prevEl: '.swiper-slide-prev',
        },
        autoHeight: true
    })

} );
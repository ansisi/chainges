import { getEl, getEls, forEach } from '@superskrypt/sutils';

export default class Video {

	constructor() {

		this.init();

	}

	config() {

		this.el = getEl( '.video-player' );
		this.video = getEl( '.video', this.el );
		this.videoRatio = 1920/1080;
		this.playButton = getEl( '.play-button', this.el );

	}

	init() {

		this.config();
		this.setVideoDimensions();
		this.initPlayButtonEvents();
		this.initVideoEvents();

	}

	setVideoDimensions() {
		this.videoContainerWidth = this.el.offsetWidth;
        this.videoContainerHeight = this.el.offsetHeight;
        this.videoContainerRatio = this.videoContainerWidth / this.videoContainerHeight;

        if (this.videoContainerRatio < this.videoRatio) {
        	this.video.classList.remove('wide');
        	this.video.classList.add('high');
        }
        else {
            this.video.classList.remove('high');
        	this.video.classList.add('wide');
        }

	}

	initPlayButtonEvents() {
		this.playButton.addEventListener( 'click', this.playButtonHandler.bind( this ) );
	}

	playButtonHandler(e) {
		e.preventDefault();
		this.video.classList.add('is-playing');
		document.body.classList.add('video-playing');
		this.video.play();
	}

	initVideoEvents() {
		this.video.addEventListener( 'ended', this.videoEndedHandler.bind( this ) );
	}

	videoEndedHandler() {
		this.video.classList.remove('is-playing');
		document.body.classList.remove('video-playing');
		this.video.currentTime = 0;
		this.video.pause();
	}

}

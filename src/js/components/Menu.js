import { getEl, getEls, forEach } from '@superskrypt/sutils';

export default class Menu {

	constructor() {

		this.init();

	}

	config() {

		this.el = getEl( '.menu-main' );
		this.burger = getEl( '.burger', this.el );
		this.menuContainer = getEl( '.menu', this.el );
		this.pageContainer = getEl( 'body' );
		this.header = getEl( '.header-main' );

	}

	init() {

		this.config();
		this.initMenuEvents();

	}

	initMenuEvents() {

        this.burger.addEventListener( 'click', this.menuClickHandler.bind( this ) );
        window.addEventListener( 'scroll', this.windowScrollEvent.bind( this ) );

	}

	menuClickHandler( e ) {
		e.stopPropagation();
		this.pageContainer.classList.toggle('menu-opened');

        if(this.pageContainer.classList.contains('menu-opened')){
            window.addEventListener( 'click', this.closeMenuIfClickOutside.bind( this ) );
        } else {
            window.removeEventListener( 'click', this.closeMenuIfClickOutside.bind( this ) );
        }

	}

    closeMenuIfClickOutside(e) {
        const isClickInside = this.menuContainer.contains(event.target);

        if (!isClickInside) {
            this.pageContainer.classList.remove('menu-opened');
        }
    }

    windowScrollEvent( e ) {

		let scrollTop = window.pageYOffset || document.documentElement.scrollTop;

		if (scrollTop >= this.header.offsetHeight) {
			this.header.classList.add('before-scrolled');
		} else {
			this.header.classList.remove('before-scrolled');
		}

		if (scrollTop >= this.header.offsetHeight * 5) {
			this.header.classList.add('is-scrolled');
		} else {
			this.header.classList.remove('is-scrolled');
		}

	}

}

import { getEl, getEls, forEach, getClosest } from '@superskrypt/sutils';

export default class Toggler {

	constructor() {

		this.init();

	}

	config() {

        this.toggleBtns = getEls( '[toggle="show"]' );

	}

	init() {

        this.config();

        if( this.toggleBtns ) {

            this.initTogglerEvent();

        }

	}

	initTogglerEvent() {

        forEach( this.toggleBtns, btn => {

            btn.addEventListener( 'click', this.toggleClickHandler.bind( this ) );

        });

	}

	toggleClickHandler( e ) {

        e.preventDefault();

        const toggleContainer = getClosest( e.target, '[toggle="container"]' );

        toggleContainer.classList[ toggleContainer.classList.contains('active') ? 'remove':'add']('active');

	}

}

import { getEl, getEls, forEach, browser } from '@superskrypt/sutils';

export default class Cookies {

	constructor() {
		this.init();
	}

	config() {
        this.supportsLocalStorage = typeof(localStorage) !== 'undefined';
        this.cookies = getEl("#cookies");
	    this.cookiesAccept = getEls(".accept");
	    this.cookieInfoStorageName = 'acceptedCookieInfo';
	    this.body = getEl('body');
	}

	init() {
        this.config();
        this.cookieInfoSetup();
        forEach( this.cookiesAccept, accept => { 
        	accept.addEventListener( 'click', this.cookiesAcceptance.bind(this) );
        } );
    }

    showCookies() {
        this.cookies.style.display = "flex";
    }

    cookieInfoSetup() {
        if (this.supportsLocalStorage && localStorage.getItem(this.cookieInfoStorageName)) {
            this.removeCookieElement();
        } else {
            this.showCookies();
        }
    }

    cookiesAcceptance() {
        this.removeCookieElement();
        if (this.supportsLocalStorage) {
            localStorage.setItem(this.cookieInfoStorageName, true);
        }
    }

    removeCookieElement() {
        this.cookies.parentNode.removeChild(this.cookies);
        this.body.className += " cookies-accepted";
    }

}

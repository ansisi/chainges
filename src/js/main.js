import Menu from './components/Menu';
import Cookies from './components/Cookies';
import Toggler from './components/Toggler';
import { browser, loadIcons } from '@superskrypt/sutils';

browser.on( 'browser:ready', () => {
	loadIcons();
	window.menu = new Menu();
	window.cookies = new Cookies();
	window.toggle = new Toggler();

} );

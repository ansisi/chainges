import Video from './components/video';
import Slider from './components/slider';
import { browser } from '@superskrypt/sutils';

browser.on( 'browser:ready', () => {
    window.video = new Video();
} );

browser.on( 'browser:resize', () => {
     window.video.setVideoDimensions();
} );
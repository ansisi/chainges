<?php

if ( ! class_exists( 'Timber' ) ){
    echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';

    return;
}

use Timber\Post;

$data            = Timber::get_context();
$data['sidebar'] = Timber::get_widgets( 'shop-sidebar' );

$front = new Post( get_option( 'page_on_front' ) );
$data['landing_content'] = $front;

$data['footer_conf_description'] = carbon_get_theme_option('footer_conf_description');
$data['footer_socials'] = carbon_get_theme_option('footer_socials');
$data['footer_contact'] = carbon_get_theme_option('footer_contact');

$data['cookies_description_before_link'] = carbon_get_theme_option('cookies_description_before_link');
$data['cookies_description_after_link'] = carbon_get_theme_option('cookies_description_after_link');
$data['cookies_link_label'] = carbon_get_theme_option('cookies_link_label');
$data['cookies_link'] = carbon_get_theme_option('cookies_link');
$data['cookies_button'] = carbon_get_theme_option('cookies_button');

$data['title'] = post_type_archive_title( '', false );
$data['currency'] = get_woocommerce_currency_symbol();

if ( is_singular( 'product' ) ) {
    $data['post']    = Timber::get_post();
    $product            = wc_get_product( $data['post']->ID );
    $data['product'] = $product;
    var_dump( 'singular' );
    var_dump( $product );
    Timber::render( 'templates/woo/single-product.twig', $data );
} else {

    $products = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
    );

    $data['products'] = Timber::get_posts( $products );

    if ( is_product_category() ) {
        $queried_object = get_queried_object();
        $term_id = $queried_object->term_id;
        $data['category'] = get_term( $term_id, 'product_cat' );
        $data['title'] = single_term_title( '', false );
    }

    Timber::render( 'templates/woo/archive.twig', $data );
}
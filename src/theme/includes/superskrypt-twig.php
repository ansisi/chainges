<?php

use Timber\Timber;
use Timber\Site;
use Timber\Menu;

// we need to initialise the timber to integrate with Wordpress
$timber = new \Timber\Timber();

Timber::$dirname = array('templates');


class KartaTheme extends Site {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		parent::__construct();
	}

	function add_to_context( $context ) {

		$assetsURI = get_assets_directory_uri();
		$context['menu'] = new Menu('main-nav');
		$context['menu2'] = new Menu('buttons-nav');
		$context['site'] = $this;
		$context['assets_url'] = $assetsURI;
		$context['img_dir'] = $assetsURI . '/images';
		$context['login_url'] = wp_login_url();
		$context['logout_url'] = wp_logout_url($_SERVER['REQUEST_URI']);

		return $context;
	}

	function objFromArray( $arr, $key, $value ) {

		if (is_array( $arr ) ) {

			$result = array_filter( $arr, function($item) use ($key, $value){

				if( $item->$key == $value ){
					return true;
				} else {
					return false;
				}

			} );
		}
		else {

			$result = array();
		}
		// just return last item from an array
		return array_pop($result);

	}

	function paragraphizle( $string ){

		return str_replace( ["\r\n",], "<br>", $string);

	}

	function real_post_date( $post ) {

		return get_the_real_post_date( $post );

	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addFilter( new Twig_SimpleFilter( 'paragraphs',  array($this, 'paragraphizle') ) );
		$twig->addFilter( new Twig_SimpleFilter( 'objFromArray', [ $this, 'objFromArray'] ) );
		$twig->addFilter( new Twig_SimpleFilter( 'real_post_date', [ $this, 'real_post_date'] ) );

		return $twig;
	}

}

new KartaTheme();


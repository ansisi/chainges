<?php

function superskrypt_scripts_and_styles() {

    wp_enqueue_style('default', get_assets_directory_uri().'/css/main.css', array());

	wp_enqueue_script('main-js', get_assets_directory_uri().'/js/main.js', array(), false, true);

	wp_register_script('front-js', get_assets_directory_uri().'/js/frontpage.js', array(), false, true);

	wp_deregister_script( 'wp-embed' );

	$to_front_array = array(
		'siteurl' 	=> get_option('siteurl'),
		'themeUrl' 	=> get_template_directory_uri(),
		'assetsUrl' => get_assets_directory_uri(),
		'frontpage' => is_front_page()
	);

	wp_localize_script( 'main-js', 'superskrypt', $to_front_array );

}

add_action('wp_enqueue_scripts', 'superskrypt_scripts_and_styles');

?>

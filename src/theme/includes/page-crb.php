<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_post_fields' );

function crb_post_fields() {

    Container::make('theme_options', 'theme_options', 'Theme Options')
    ->add_fields( array(
        Field::make('textarea', 'footer_conf_description', 'Footer conf description'),
        Field::make( 'complex', 'footer_socials', 'Footer socials' )
            ->set_layout('tabbed-horizontal')
            ->setup_labels( array(
                'plural_name' => 'Footer socials',
                'singular_name' => 'Footer social',
            ) )
            ->add_fields( array(
                Field::make( 'text', 'footer_social_name', 'Footer social name' ),
                Field::make( 'file', 'footer_social_image', 'Footer social image' )
                    ->set_type( 'image' )
                    ->set_value_type( 'url' ),
                Field::make( 'text', 'footer_social_link', 'Footer social link' )
                    ->set_attribute( 'type', 'url' ),
            ) ),
        Field::make( 'text', 'footer_contact', 'Footer contact' )
                    ->set_attribute( 'type', 'email' ),
        ) )
    ->add_fields( array(
        Field::make('textarea', 'cookies_description_before_link', 'Cookies description before link'),
        Field::make('textarea', 'cookies_description_after_link', 'Cookies description after link'),
        Field::make( 'text', 'cookies_link_label', 'Cookies link label' ),
        Field::make( 'text', 'cookies_link', 'Cookies link' )
            ->set_attribute( 'type', 'url' ),
        Field::make( 'text', 'cookies_button', 'Cookies button' )
    ) );  

}

<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_term_fields' );

function crb_term_fields() {

	Container::make( 'term_meta', 'Category Properties' )
    ->where( 'term_taxonomy', '=', 'project_category' )
    ->add_fields( array(
        Field::make( 'image', 'term_thumb', 'Tło kategorii' )
        ->set_value_type( 'url' ),

    ) );

}
<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_frontpage_fields' );

function crb_frontpage_fields() {

    Container::make( 'post_meta', 'landing_content', 'Landing content' )
        ->where( 'post_id', '=', get_option( 'page_on_front' ) )
        ->add_fields( array(
        	Field::make( 'file', 'landing_image', 'Landing image' )
            ->set_type( 'image' )
            ->set_value_type( 'url' )
            ->set_required( true ),
            Field::make( 'text', 'event_details', 'Event details' ),
            Field::make( 'text', 'chainges_motto', 'Chainges motto' )
            ->set_required( true ),
        ));

    Container::make( 'post_meta', 'About content' )
        ->where( 'post_id', '=', get_option( 'page_on_front' ) )
        ->add_fields( array(
            Field::make( 'file', 'about_image', 'About image' )
			->set_type( 'image' )
			->set_value_type( 'url' ),
            Field::make( 'text', 'about_header', 'About header' )
            ->set_required( true ),
            Field::make( 'textarea', 'about_description', 'About description' )
			->set_rows( 15 ),
        ));

    Container::make( 'post_meta', 'Link tiles' )
        ->where( 'post_id', '=', get_option( 'page_on_front' ) )
        ->add_fields( array(
			Field::make( 'complex', 'link_tiles', 'Link tiles' )
				->set_layout('tabbed-horizontal')
				->set_max( 4 )
				->setup_labels( array(
				    'plural_name' => 'Link tiles',
				    'singular_name' => 'Link tile',
				) )
				->add_fields( array(
					Field::make( 'text', 'link_tile_title', 'Link tile title' ),
					Field::make( 'textarea', 'link_tile_description', 'Link tile description' )
					->set_rows( 15 ),
					Field::make( 'text', 'link_tile_link', 'Link tile link' )
						->set_attribute( 'type', 'url' ),
				) ),
		) );

	Container::make( 'post_meta', 'Partners section' )
        ->where( 'post_id', '=', get_option( 'page_on_front' ) )
       	->add_fields( array(
			Field::make( 'text', 'partners_section_title', 'Partners section title' ),
			Field::make( 'textarea', 'partners_section_description', 'Partners section description' )
		) );

	Container::make( 'post_meta', 'Speakers section' )
        ->where( 'post_id', '=', get_option( 'page_on_front' ) )
       	->add_fields( array(
			Field::make( 'text', 'speakers_section_title', 'Speakers section title' )
		) );

	Container::make( 'post_meta', 'Offer tiles' )
        ->where( 'post_id', '=', get_option( 'page_on_front' ) )
        ->add_fields( array(
        	Field::make( 'text', 'offer_tiles_header', 'Offer tiles header' ),
        	Field::make( 'textarea', 'offer_tiles_description', 'Offer tiles description' ),
			Field::make( 'complex', 'offer_tiles', 'Offer tiles' )
				->set_layout('tabbed-horizontal')
				->set_max( 6 )
				->setup_labels( array(
				    'plural_name' => 'Offer tiles',
				    'singular_name' => 'Offer tile',
				) )
				->add_fields( array(
					Field::make( 'text', 'offer_tile_title', 'Offer tile title' ),
					Field::make( 'file', 'offer_tile_image', 'Offer tile image' )
					->set_value_type( 'url' ),
					Field::make( 'text', 'offer_tile_link', 'Offer tile link' )
						->set_attribute( 'type', 'url' ),
				) ),
		) );

	Container::make( 'post_meta', 'Testimonials section' )
        ->where( 'post_id', '=', get_option( 'page_on_front' ) )
       	->add_fields( array(
			Field::make( 'text', 'testimonials_section_title', 'Testimonials section title' ),
			Field::make( 'textarea', 'testimonials_section_summary', 'Testimonials section summary' ),
		) );
}


add_action( 'after_setup_theme', 'crb_load' );

function crb_load() {

    \Carbon_Fields\Carbon_Fields::boot();

}
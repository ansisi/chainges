<?php

// we're firing all out initial functions at the start
add_action( 'after_setup_theme', 'superskrypt_ahoy', 16 );

function superskrypt_ahoy() {

	// launching operation cleanup
	add_action( 'init', 'superskrypt_head_cleanup' );
	// remove WP version from RSS
	add_filter( 'the_generator', 'superskrypt_rss_version' );
	// remove pesky injected css for recent comments widget
	add_filter( 'wp_head', 'superskrypt_remove_wp_widget_recent_comments_style', 1 );
	// clean up comment styles in the head
	add_action( 'wp_head', 'superskrypt_remove_recent_comments_style', 1 );
	// clean up gallery output in wp
	add_filter( 'gallery_style', 'superskrypt_gallery_style' );

	// enqueue frontend scripts and styles
	add_action( 'wp_enqueue_scripts', 'superskrypt_scripts_and_styles', 5);

	// enable svg upload in WP backend
	add_filter('upload_mimes', 'cc_mime_types');

	// fix svg display in WP admin
	add_action('admin_head', 'custom_admin_head');

	// launching this stuff after theme setup
	superskrypt_theme_support();

	// cleaning up random code around images
	add_filter( 'the_content', 'superskrypt_filter_ptags_on_images' );
	// cleaning up excerpt
	add_filter( 'excerpt_more', 'superskrypt_excerpt_more' );

    add_filter('show_admin_bar', '__return_false');

} /* end superskrypt ahoy */

/*********************
WP_HEAD GOODNESS
The default wordpress head is
a mess. Let's clean it up by
removing all the junk we don't
need.
*********************/

function superskrypt_head_cleanup() {
	// category feeds
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// index link
	remove_action( 'wp_head', 'index_rel_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
	// remove WP version from css
	add_filter( 'style_loader_src', 'superskrypt_remove_wp_ver_css_js', 9999 );
	// remove Wp version from scripts
	add_filter( 'script_loader_src', 'superskrypt_remove_wp_ver_css_js', 9999 );

	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

	wp_deregister_script( 'comment-reply' );

} /* end fbi head cleanup */

// remove WP version from RSS
function superskrypt_rss_version() { return ''; }

// remove WP version from scripts
function superskrypt_remove_wp_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}

// remove injected CSS for recent comments widget
function superskrypt_remove_wp_widget_recent_comments_style() {
	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	}
}

// remove injected CSS from recent comments widget
function superskrypt_remove_recent_comments_style() {
	global $wp_widget_factory;
	if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
		remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
	}
}

// remove injected CSS from gallery
function superskrypt_gallery_style($css) {
	return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

/*********************
THEME SUPPORT
*********************/

function custom_admin_head() {
  $css = '';

  $css = 'td.media-icon img[src$=".svg"] { width: 100% !important; height: auto !important; }';

  echo '<style type="text/css">'.$css.'</style>';
}

// Adding WP 3+ Functions & Theme Support
function superskrypt_theme_support() {

	// wp thumbnails (sizes handled in functions.php)
	add_theme_support( 'post-thumbnails' );

	add_theme_support('woocommerce');

	// default thumb size
	//set_post_thumbnail_size(125, 125, true);

	// wp custom background (thx to @bransonwerner for update)
	add_theme_support( 'custom-background',
	    array(
	    'default-image' => '',    // background image default
	    'default-color' => '',    // background color default (dont add the #)
	    'wp-head-callback' => '_custom_background_cb',
	    'admin-head-callback' => '',
	    'admin-preview-callback' => ''
	    )
	);

	// wp menus
	add_theme_support( 'menus' );

	// registering wp3+ menus
	register_nav_menus(
		array(
			'main-nav' => __( 'The Main Menu', 'innohuman-theme' ),   // main nav in header
			'buttons-nav' => __( 'The Secondary Menu', 'innohuman-theme' ),   // main nav in header
		)
	);
} /* end fbi theme support */


/*********************
PAGE NAVI
*********************/

// Numeric Page Navi (built into the theme by default)
function superskrypt_page_navi() {
  global $wp_query;
  $bignum = 999999999;
  if ( $wp_query->max_num_pages <= 1 )
    return;
  echo '<nav class="pagination">';
  echo paginate_links( array(
    'base'         => str_replace( $bignum, '%#%', esc_url( get_pagenum_link($bignum) ) ),
    'format'       => '',
    'current'      => max( 1, get_query_var('paged') ),
    'total'        => $wp_query->max_num_pages,
    'prev_text'    => '&larr;',
    'next_text'    => '&rarr;',
    'type'         => 'list',
    'end_size'     => 3,
    'mid_size'     => 3
  ) );
  echo '</nav>';
} /* end page navi */

/*********************
RANDOM CLEANUP ITEMS
*********************/

// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
function superskrypt_filter_ptags_on_images($content){
	return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

// This removes the annoying […] to a Read More link
function superskrypt_excerpt_more($more) {
	global $post;
	// edit here if you like
	return '...  <a class="excerpt-read-more" href="'. get_permalink($post->ID) . '" title="'. __( 'Read ', 'innohuman-theme' ) . get_the_title($post->ID).'">'. __( 'Read more &raquo;', 'innohuman-theme' ) .'</a>';
}

/**
 * 	Disable Rss
 */
function superskrypt_disable_feed() {
	wp_die( __('No feed available,please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
}
add_action('do_feed', 'superskrypt_disable_feed', 10, 1);
add_action('do_feed_rdf', 'superskrypt_disable_feed', 10, 1);
add_action('do_feed_rss', 'superskrypt_disable_feed', 10, 1);
add_action('do_feed_rss2', 'superskrypt_disable_feed', 10, 1);
add_action('do_feed_atom', 'superskrypt_disable_feed', 10, 1);

remove_action( 'wp_head', 'feed_links_extra', 3 ); //Extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // General feeds: Post and Comment Feed

/**
 * ASYNC SCRIPTS LOADING
 *
 * When registering add '#asyncload' on the end of file path
 * wp_register_script( 'fbi-pinit', 'http://assets.pinterest.com/js/pinit.js#asyncload', array(), '0.2.0', true );
 */
add_filter('clean_url','async_loading',10,3);

function async_loading( $good_protocol_url, $original_url, $_context){
	//var_dump($good_protocol_url);

    if ( true == strpos($original_url, '#asyncload')){
      	//remove_filter('clean_url','async_loading',10,3);
      	$url_parts = parse_url($good_protocol_url);
      	$port = isset($url_parts['port']) ? ':' . $url_parts['port'] : '' ;
      	return $url_parts['scheme'] . '://' . $url_parts['host'] . $port . $url_parts['path'] . "' async ";
    }
    else if (true == strpos($original_url, '#deferload')){
    	//remove_filter('clean_url','async_loading',10,3);
      	$url_parts = parse_url($good_protocol_url);
      	$port = isset($url_parts['port']) ? ':' . $url_parts['port'] : '' ;
      	return $url_parts['scheme'] . '://' . $url_parts['host'] . $port . $url_parts['path'] . "' defer ";

    }
    return $good_protocol_url;
}

/**
 * 	ADD  OPTIONS PAGE
 */

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

function superskrypt_is_local(){
	if ( strpos(site_url(), '.0.') !== false || strpos(site_url(), 'localhost') !== false ){
		return true;
	} else {
		return false;
	}

}

function superskrypt_hide_editor_and_excerpt() {
	// remove_post_type_support( 'page', 'editor' );
	remove_post_type_support( 'page', 'excerpt' );
}

add_action('admin_init', 'superskrypt_hide_editor_and_excerpt', 2);


function superskrypt_hide_excerpt_for_links_page() {

    if(isset($_GET['post']) ){

        $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;

        if( !isset( $post_id ) ) return;

        $template_file = get_post_meta($post_id, '_wp_page_template', true);

        if ( $template_file == 'page-links.php' ) {
            remove_post_type_support( 'page', 'excerpt' );
        }
    }
}

add_action( 'admin_init', 'superskrypt_hide_excerpt_for_links_page' );

function get_assets_directory_uri() {

	$template = str_replace( '%2F', '/', rawurlencode( get_template() ) );
	$theme_root_uri = get_theme_root_uri( $template );
	$template_dir_uri = "$theme_root_uri/$template";

	return $theme_root_uri . '/' . explode( '/', $template )[0];

}

function get_the_real_post_date( $post, $datef = 'M j, Y @ G:i') {

    if ( !empty( $timezone_string = get_option( 'timezone_string' ) ) )  {

        $timezone_object = timezone_open( $timezone_string );
        $datetime_object = date_create( $post->post_date_gmt );

        $offset_sec = round( timezone_offset_get( $timezone_object, $datetime_object ) );
        // if you want $offset_hrs = round( $offset_sec / 3600 );

        return date_i18n( $datef, strtotime( $post->post_date_gmt ) + $offset_sec );

    } elseif (!empty( $offset_hrs = get_option('gmt_offset') ) ) {

        // this option shows empty for me so I believe it's only used by WP pre 3.0
        // the option stores an integer value for the time offset in hours

        $offset_sec = $offset_hrs * 3600;
        return date_i18n( $datef, strtotime( $post->post_date_gmt ) + $offset_sec );

    } else {

        return;  // shouldn't happen but...

    }
}

?>

<?php

// Allow editors to see Appearance menu
$role_object = get_role( 'editor' );
$role_object->add_cap( 'edit_theme_options' );
function hide_menu() {

	if( ! current_user_can('administrator') ) {

    // Hide theme selection page
	    remove_submenu_page( 'themes.php', 'themes.php' );
	 
	    // Hide widgets page
	    remove_submenu_page( 'themes.php', 'widgets.php' );
	 
	    // Hide customize page
	    global $submenu;
	 //    echo'<pre>';
		// print_r($submenu);
		// 	echo'</pre>';
	    unset($submenu['themes.php'][6]);
	    unset($submenu['themes.php'][20]);
	    unset($submenu['themes.php'][21]);
	    
	}
	remove_menu_page( 'edit.php' ); 
	remove_menu_page( 'edit-comments.php' ); 
 
}
 
add_action('admin_head', 'hide_menu');
return;
function block_admin_access() {

	if ( is_admin() && ! current_user_can( 'administrator' ) &&
		! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
		wp_redirect( home_url() );
		exit;
	}

}

add_action( 'admin_init', 'block_admin_access' );
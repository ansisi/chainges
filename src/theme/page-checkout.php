<?php

use Timber\Timber;
use Timber\Post;

$post = new Post();
$data = Timber::get_context();
$templates = array( 'page-checkout.twig' );

$data['post'] = $post;

$front = new Post( get_option( 'page_on_front' ) );
$data['landing_content'] = $front;

$data['footer_conf_description'] = carbon_get_theme_option('footer_conf_description');
$data['footer_socials'] = carbon_get_theme_option('footer_socials');
$data['footer_contact'] = carbon_get_theme_option('footer_contact');

$data['cookies_description_before_link'] = carbon_get_theme_option('cookies_description_before_link');
$data['cookies_description_after_link'] = carbon_get_theme_option('cookies_description_after_link');
$data['cookies_link_label'] = carbon_get_theme_option('cookies_link_label');
$data['cookies_link'] = carbon_get_theme_option('cookies_link');
$data['cookies_button'] = carbon_get_theme_option('cookies_button');

Timber::render( $templates, $data );

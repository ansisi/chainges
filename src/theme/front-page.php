<?php

add_action( 'wp_enqueue_scripts', function () {
	wp_enqueue_script('front-js');
    wp_enqueue_script( 'front-js', get_assets_directory_uri(). '/js/' . 'front-page.js', array(), '1', true );
} );

use Timber\Timber;
use Timber\Post;

$post = new Post();
$data = Timber::get_context();
$templates = array( 'front-page.twig' );

$data['post'] = $post;
$data['landing_content'] = $post;

$partners_args = array(
    'post_type' => 'partner',
    'posts_per_page' => 12,
);

$data['partners_posts'] = Timber::get_posts( $partners_args );
$args = array(
    'post_type' => 'speaker',
    'posts_per_page' => 16,
);

$data['speakers'] = Timber::get_posts( $args );

$data['footer_conf_description'] = carbon_get_theme_option('footer_conf_description');
$data['footer_socials'] = carbon_get_theme_option('footer_socials');
$data['footer_contact'] = carbon_get_theme_option('footer_contact');

$data['cookies_description_before_link'] = carbon_get_theme_option('cookies_description_before_link');
$data['cookies_description_after_link'] = carbon_get_theme_option('cookies_description_after_link');
$data['cookies_link_label'] = carbon_get_theme_option('cookies_link_label');
$data['cookies_link'] = carbon_get_theme_option('cookies_link');
$data['cookies_button'] = carbon_get_theme_option('cookies_button');

$args = array(
    'post_type' => 'testimonial',
    'posts_per_page' => -1,
);

$data['testimonials'] = Timber::get_posts( $args );

Timber::render( $templates, $data );

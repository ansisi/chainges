<?php

namespace Chainges;

use Carbon_Fields\Container as Container;
use Carbon_Fields\Field as Field;

class Partner_CPT {
    
    public function register_custom_post_type() {

        register_post_type( 'partner',
            array(
                'labels' => array(
                    'name' => __( 'Partners', 'Chainges' ),
                    'singular_name' => __( 'Partner', 'Chainges' ),
                    'all_items' => __( 'All Partners', 'Chainges' ),
                    'parent_item_colon' => ''
                ),
                'public' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => false,
                'query_var' => true,
                'menu_position' => 20,
                'hierarchical' => false,
                'rewrite' => array('slug' => 'partners'),
                'supports' => array( 'title', 'thumbnail' ),
                'taxonomies' => array( 'category' ),
                'menu_icon'     => 'dashicons-groups',
            )
        );
    }
    
    public function register_carbon_fields() {
        Container::make( 'post_meta', 'partner fields' )
            ->where( 'post_type', '=', 'partner' )
            ->add_fields(array(
                Field::make( 'text', 'partner_link', 'Partner link' )
                        ->set_attribute( 'type', 'url' ),
            ));

    }
}

$partner_cpt = new \Chainges\Partner_CPT();
add_action('init', array($partner_cpt, 'register_custom_post_type'));
add_action('carbon_fields_register_fields', array($partner_cpt, 'register_carbon_fields'));

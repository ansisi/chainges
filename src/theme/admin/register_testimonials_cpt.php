<?php

namespace Chainges;

use Carbon_Fields\Container as Container;
use Carbon_Fields\Field as Field;

class Testimonial_CPT {

    public function register_custom_post_type() {

        register_post_type( 'testimonial',
            array(
                'labels' => array(
                    'name' => __( 'Testimonials', 'Chainges' ),
                    'singular_name' => __( 'Testimonial', 'Chainges' ),
                    'all_items' => __( 'All Testimonials', 'Chainges' ),
                    'parent_item_colon' => ''
                ),
                'public' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => false,
                'query_var' => true,
                'menu_position' => 20,
                'hierarchical' => false,
                'rewrite' => array('slug' => 'testimonials'),
                'supports' => array( 'title', 'editor', 'thumbnail' ),
                'menu_icon' => 'dashicons-format-quote',
            )
        );
    }

    public function register_carbon_fields() {

        Container::make( 'post_meta', 'testimonial fields' )
        ->where( 'post_type', '=', 'testimonial' )
        ->add_fields( array(
            Field::make( 'text', 'testimonial_job_position', 'Job position' ),
            Field::make( 'text', 'testimonial_company', 'Company' ),
        ));

    }
}

$testimonial_cpt = new \Chainges\Testimonial_CPT();
add_action('init', array($testimonial_cpt, 'register_custom_post_type'));
add_action('carbon_fields_register_fields', array($testimonial_cpt, 'register_carbon_fields'));

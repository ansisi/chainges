<?php
 
add_action( 'woocommerce_review_order_before_submit', 'checkout_terms_and_privacy_policy', 9 );
   
function checkout_terms_and_privacy_policy() {
  
woocommerce_form_field( 'privacy_policy', array(
    'type'          => 'checkbox',
    'class'         => array('form-row privacy'),
    'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
    'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
    'required'      => true,
    'label'         => 'By buying a ticket, you agree to the <a href="{{ img_dir }}/Terms_and_Conditions_-_Chainges.pdf" download target="_blank">Terms and Conditions</a> and <a href="/privacy-policy" target="_blank">Privacy Policy</a> of Chainges',
)); 
  
}
   
add_action( 'woocommerce_checkout_process', 'checkout_not_accepted_terms_and_privacy' );
  
function checkout_not_accepted_terms_and_privacy() {
    if ( ! (int) isset( $_POST['privacy_policy'] ) ) {
        wc_add_notice( __( 'Please acknowledge the Terms and Conditions and the Privacy Policy' ), 'error' );
    }
}

?>
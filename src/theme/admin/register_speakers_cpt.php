<?php

namespace Chainges;

use Carbon_Fields\Container as Container;
use Carbon_Fields\Field as Field;

class Speaker_CPT {

    public function register_custom_post_type() {

        register_post_type( 'speaker',
            array(
                'labels' => array(
                    'name' => __( 'Speakers', 'Chainges' ),
                    'singular_name' => __( 'Speaker', 'Chainges' ),
                    'all_items' => __( 'All Speakers', 'Chainges' ),
                    'parent_item_colon' => ''
                ),
                'public' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => false,
                'query_var' => true,
                'menu_position' => 20,
                'hierarchical' => false,
                'rewrite' => array('slug' => 'speakers'),
                'supports' => array( 'title', 'editor', 'thumbnail' ),
                'taxonomies' => array( 'category' ),
                'menu_icon' => 'dashicons-universal-access',
            )
        );
    }

    public function register_carbon_fields() {

        Container::make( 'post_meta', 'speaker fields' )
        ->where( 'post_type', '=', 'speaker' )
        ->add_fields( array(
            Field::make( 'text', 'speaker_job_position', 'Job position' ),
            Field::make( 'text', 'speaker_company', 'Company' ),
            Field::make( 'text', 'speaker_country', 'Country' ),
            Field::make( 'text', 'speaker_facebook', "Facebook" ),
            Field::make( 'text', 'speaker_linkedin', "LinkedIn" ),
            Field::make( 'text', 'speaker_twitter', "Twitter" ),
        ));

    }
}

$speaker_cpt = new \Chainges\Speaker_CPT();
add_action('init', array($speaker_cpt, 'register_custom_post_type'));
add_action('carbon_fields_register_fields', array($speaker_cpt, 'register_carbon_fields'));

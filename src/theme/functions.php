<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

function print_r2( $val ) {
    echo '<pre>';
    print_r($val);
    echo '</pre>';
}


if (!function_exists('write_log')) {
    function write_log ( $log )  {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( print_r( $log, true ) );
            } else {
                error_log( $log );
            }
        }
    }
}

/**
 * Composer Dependencies
 */
require_once( __DIR__ . '/../vendor/autoload.php');

require_once( 'includes/Timber_Integrations_Overwrite.php');
require_once( 'includes/Timber_CarbonFields.php');

use Superskrypt\WordpressThemeBase as wtb;

function init_theme_ss() {
	wtb\WordpressThemeBase:: initTheme();
}

function admin_head_ss() {
	wtb\WordpressThemeBase:: adminHead();
}

function admin_footer_ss() {
	wtb\WordpressThemeBase:: adminFooter();
}


add_action( 'init', 'init_theme_ss' );

add_action('admin_head', 'admin_head_ss');

add_action('admin_footer', 'admin_footer_ss');

require('includes/superskrypt-twig.php');

require('includes/superskrypt.php');

require('includes/superskrypt-admin.php');

require('includes/enqueue-styles-scripts.php');

include "includes/page-crb.php";

include "includes/frontpage-crb.php";

include "admin/register_partners_cpt.php";

include "admin/register_speakers_cpt.php";

include "admin/register_testimonials_cpt.php";

include "admin/add_checkboxes_to_checkout.php";

function timber_set_product( $post ) {
    global $product;

    if ( is_woocommerce() ) {
        $product = wc_get_product( $post->ID );
    }
}


